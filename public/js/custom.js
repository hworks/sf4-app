function onChange(event) {
    var image = event.target.files[0];
    var imagePreview = document.getElementById('image-preview');
    imagePreview.src = window.URL.createObjectURL(image);   
}