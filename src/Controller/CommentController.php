<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\EntityManager;
use App\Entity\Article;
use App\Entity\Comment;
use App\Form\CommentType;
use Symfony\Component\HttpFoundation\Request;

class CommentController extends Controller
{
    
    /**
     * @Route("/comment/{slug}", defaults={"slug" = null}, name="comment")
     */
    public function comment(Request $request, $slug)
    {

        $comment = new Comment();

        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(CommentType::class, $comment);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $comment = $form->getData();
            $user = $this->getUser();
            $article = $em->getRepository(Article::class)->findOneBy(array('slug' => $slug));

            $comment->setUser($user);
            $comment->setArticle($article);

            $comment->setCreatedAt(new \DateTime("now"));

            $em->persist($comment);
            $em->flush();

            return $this->redirectToRoute('article', [
                'article' => $article,
                'slug'  => $article->getSlug()
            ]);
        }

        return $this->render('blog/comment.html.twig', array(
            'form' => $form->createView(),
            'slug' => $slug
        ));
        
    }


}
