<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\EntityManager;
use App\Entity\Article;
use App\Entity\Comment;
use Symfony\Component\HttpFoundation\Request;
use Knp\Component\Pager\PaginatorInterface;

class ArticleController extends Controller
{
    
    /**
     * @Route("/{page}", requirements={"page" = "\d+"}, name="home")
     */
    public function index(Request $request, PaginatorInterface $paginator, $page = 1)
    {

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository(Article::class);

        $dql   = "SELECT a FROM App:Article a";
        $query = $em->createQuery($dql);

        $pagination = $paginator->paginate(
            $query,                             /* query NOT result */
            $page,                              /* page number */
            4                                   /* limit per page */
        );
    
        return $this->render('blog/index.html.twig', array('pagination' => $pagination));
        
    }


    /**
     * @Route("/article/{slug}", name="article")
     */
    public function article(string $slug)
    {
        $em = $this->getDoctrine()->getManager();
        $article_repository = $em->getRepository(Article::class);
        $comment_repository = $em->getRepository(Comment::class);

        $article = $article_repository->findOneBy([
            'slug' => $slug
        ]);

        $comments = $comment_repository->findBy([
            'article' => $article
        ]);

        return $this->render('blog/article.html.twig', array(
            'article' => $article,
            'comments' => $comments
        ));
    }

}
