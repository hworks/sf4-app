<?php

namespace App\Form;

use App\Entity\Article;
use App\Entity\Category;
use App\Entity\Tag;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('subtitle')
            ->add('content', TextareaType::class)
            ->add('category', EntityType::class, array(
                'class' => Category::class,
            ))
            ->add('tags', EntityType::class, array(
                'class' => Tag::class,
                'multiple' => true
            ))
            ->add('image', FileType::class, array(
                'data_class' => null,
                'required' => false,
                'attr' => array ( 'onchange' => 'onChange(event)')
            ))
            // ->add('slug')
            // ->add('publishedAt')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
        ]);
    }
}
